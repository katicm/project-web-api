import express from "express";
import request from "request-promise";
import authenticate from "../middlewares/authenticate";
import Movie from "../models/Movie";
import parseErrors from "../utils/parseErrors";
//xml2js
const router = express.Router();
router.use(authenticate);

router.get("/", (req, res) => {
  Movie.find({ userId: req.currentUser._id }).then(movies =>
    res.json({ movies })
  );
});

router.post("/", (req, res) => {
  Movie.create({ ...req.body.movie, userId: req.currentUser._id })
    .then(movie => res.json({ movie }))
    .catch(err => res.status(400).json({ errors: parseErrors(err.errors) }));
});

router.post("/delete", (req, res) => {
  Movie.findOneAndRemove({ _id: req.body.id })
    .then(movie => res.json({ movie }))
    .catch(err => res.status(400).json({ errors: parseErrors(err.errors) }));
});

router.get("/search", (req, res) => {
  request
    .get(
      `https://api.themoviedb.org/3/search/movie?api_key=${
        process.env.TMDB_KEY
      }&language=en-US&query=${req.query.q}&page=1&include_adult=true`
    )
    .then(result => {
      var temp = JSON.parse(result);
      res.json({
        movies: temp.results.map(result => ({
          tMDBId: result.id,
          title: result.title,
          score: result.vote_average,
          original_title: result.original_title,
          release_date: result.release_date,
          overview: result.overview,
          cover: "https://image.tmdb.org/t/p/w500" + result.poster_path
        }))
      });
    });
});
router.get("/credits", (req, res) => {
  request
    .get(
      `https://api.themoviedb.org/3/movie/${req.query.q}/credits?api_key=${
        process.env.TMDB_KEY
      }`
    )
    .then(result => {
      var temp = JSON.parse(result);
      var cast = temp.cast.slice(0, 5);
      var crew = temp.crew.filter(director => director.job === "Director");
      res.json({
        credits: {
          actors: cast.map(actor => ({
            character: actor.character,
            gender: actor.gender,
            name: actor.name,
            profile_picture:
              actor.profile_path !== null
                ? "https://image.tmdb.org/t/p/w92" + actor.profile_path
                : null
          })),
          director: {
            name: crew[0].name,
            gender: crew[0].gender,
            profile_picture:
              crew[0].profile_path !== null
                ? "https://image.tmdb.org/t/p/w92" + crew[0].profile_path
                : null
          }
        }
      });
    });
});

export default router;
