import mongoose from "mongoose";
//model u db
const schema = new mongoose.Schema(
  {
    tMDBId: { type: Number, required: true },
    title: { type: String, required: true },
    score: { type: Number, required: true },
    release_date: { type: String, required: true },
    overview: { type: String },
    flag: { type: String },
    cover: { type: String },
    actors: [
      {
        character: { type: String },
        name: { type: String },
        profile_picture: { type: String },
        gender: { type: Number },
        _id: false
      }
    ],
    director: {
      name: { type: String },
      profile_picture: { type: String },
      gender: { type: Number }
    },
    userId: { type: mongoose.Schema.Types.ObjectId, required: true }
  },
  { versionKey: false }
);

export default mongoose.model("Movie", schema);
